# FreeDOS Help (HTML)

HTML viewer and content for FreeDOS help


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## HTMLHELP.LSM

<table>
<tr><td>title</td><td>FreeDOS Help (HTML)</td></tr>
<tr><td>version</td><td>1.0.8b</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-03-06</td></tr>
<tr><td>description</td><td>HTML viewer and content for FreeDOS help</td></tr>
<tr><td>keywords</td><td>help, html</td></tr>
<tr><td>author</td><td>Joe Cosentino (creator of the viewer)</td></tr>
<tr><td>maintained&nbsp;by</td><td>W. Spiegl, fritz - dot - mueller - AT - mail -dot - com</td></tr>
<tr><td>primary&nbsp;site</td><td>http://home.mnet-online.de/willybilly/</td></tr>
<tr><td>platforms</td><td>DOS, Turbo C++; Html</td></tr>
<tr><td>copying&nbsp;policy</td><td>[Express+GNU Free Documentation License](LICENSE)</td></tr>
</table>
